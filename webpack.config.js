const webpack = require('webpack'); // on a besoin de webpack à cause du plugin webpack.HotModuleReplacementPlugin() vous le verrez plus tard
const ExtractTextPlugin = require('extract-text-webpack-plugin'); // là encore on a un plugin, on doit le require.

/*
  Les loaders n'ont pas besoin d'être require, c'est le programme webpack lui même qui s'en chargera.
*/

module.exports = { 
  // On définit l'entrée, le javascript qui va importer tous les autres, 
  // C'est le début de la chaine de compilation.
  // webpack va appliquer les rules qu'on définit ensuite à tout ce qui est contenu ou appelé par ce fichier 
  // tous les imports, tous les require, tout ça se verra transformé par les rules webpack : 
  entry: './src/index.js',

  // On définit notre sortie, le fichier javascript compilé pour le navigateur:
  output: {
    path: __dirname + '/dist', 
    // On va mettre notre fichier compilé dans /dist
    // __dirname nous permet de prendre le chemin absolu de / depuis ce fichier et non depuis l'endroit qui appelle ce fichier, c'est une sécurité
    publicPath: '/',
    // Le public path c'est les assets ... dans un environnement de production on mettrait /static par exemple
    filename: 'bundle.js'
    // Le nom du fichier JS qui sera compilé par webpack. ./src/index.js => webpack => ./dist/bundle.js
  },

  // Pour webpack-dev-server:
  devServer: {
    hot: true, // Le serveur se relance chaque fois qu'il y a un changement dans son contentBase
    contentBase: __dirname + '/dist', // Le dossier que le serveur peut rendre aux navigateurs qui l'appellent 
    port: 3000
  },

  module: {
    rules: [ // Notre tableau de règles de compilation, on en a un peu parlé au dessus. Ici j'ai deux règles:
      {
        test: /\.(js)$/, // Tout ce qui concerne des fichiers JS ...
        exclude: /(node_modules)/, // ... mais qui ne sont pas des fichiers JS inclus dans node_modules ...
        use: [
          {
            loader: 'babel-loader', // ... devront être compilé par babel ...
            options: {
              "presets": [
                "@babel/preset-env", // ... pour changer l'es6 en langage lisible par les navigateurs ...
                "@babel/preset-react" // ... et pour changer le jsx de react en du JS qui transforme le DOM.
              ]     
            }
          }
        ], 
      },
      {
        test: /\.css$/, // Tout ce qui concerne les fichiers css ...
        use: ExtractTextPlugin.extract( // ... le fichier sera lu comme une string, on ne change rien dedans ...
          {
            fallback: 'style-loader', // ... après avoir utilisé css-loader, on utilisera style-loader pour créer le link dans le html ...
            use: ['css-loader'] // ... mais d'abord on utilise css-loader pour vérifier la syntaxe css.
          })
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.json'] // Quand on fait un import, si on ne met pas d'extension, c'est que c'est soit un fichier js, soit un fichier json.
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(), // Permet juste de relancer une compilation webpack si un fichier à été modifié. 
    new ExtractTextPlugin({filename: 'style.css'}) // Les fichiers css seront mis bout à bout dans un seul fichier style.css
  ],
};