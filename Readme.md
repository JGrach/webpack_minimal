# Webpack Minimal avec, en exemple, React

## introduction

Vous le savez déjà mais ça ne fait jamais de mal de se le faire rappeler, alors on va simplement redéfinir le rôle des outils qu'on utilise:

### Webpack

Si je code pour du navigateur en utilisant du sass par exemple, je vais devoir recompiler le sass en css avant de l'envoyer au navigateur.
Théoriquement ce que je ferais sans webpack c'est qu'après avoir écrit en sass, j'utiliserais le sass-cli pour convertir mes fichiers sass en un ou plusieurs fichiers css que je mettrais dans mon dossier static. Ce sont ces fichiers css que je vais lier à mon html de sortes à les envoyer au navigateur.

Mais ça pose plusieurs problèmes. 
1. Si j'ai plus d'un seul outil (disons du sass, mais aussi du jsx à recompiler, de l'es6, etc ...) je devrais utiliser autant de commande bash que d'outils.
2. Si j'ai plusieurs outils, il faudra des fichiers de configuration, je n'ai pas forcément envie de voir la racine de mon projet devenir une liste de fichiers de configuration cachés, je pourrais le faire dans le package.json mais ce dernier pourrait vite devenir illisible.
3. Si certains outils se servent des mêmes fichiers, je devrais peut être en gérer l'ordre, c'est à dire bien penser à chaque fois à recompiler une première fois mon js avec quelque chose avant de compiler la sortie une seconde fois. Si l'ordre est inversé je pourrais avoir une erreur, la galère.

Avec Webpack on va pouvoir gérer toutes ces recompilation en une seule config, on appellera webpack une fois, il recompilera tout ce qu'on lui aura dit de recompiler dans l'ordre dans lequel on le lui aura demandé. Pratique.

### Babel

Babel, c'est à JS ce que Sass est à Css.
Babel est un outil qui me permet de recompiler un JS écrit en ES6 en un JS lisible par tous les navigateurs.
Notons que dans le cas de react par exemple, c'est lui qui nous permet d'écrire les import et l'html sous forme de jsx, quand bien même n'utiliserions nous pas les fonctions fléchées ou la déconstruction.
C'est donc un des outils de recompilation que nous appelerons avec webpack.

### React, Angular ou Vue

De fait react, angular et vue ne sont que des moteurs de rendus permettant de modifier, voire de créer un html à partir de JS.
On pourrait théoriquement les utiliser sans webpack ou babel mais on ne pourrait plus suivre leur conventions et bonne pratiques, on ne le fait donc tout simplement pas.

## L'installation des outils

### webpack

Si on regarde le package.json présent, on peut retenir la présence de trois outils webpack:
- webpack: Bon ben là c'est webpack lui même, je vois pas trop quoi en dire, c'est le programme, c'est essentiel.
- webpack-cli: On pourrait s'en passer mais ce serait à nous d'appeler le programme webpack dans les bonne conditions avec les bons arguments, webpack-cli facilite simplement ces appels en nous fournissant une command-line.
- webpack-dev-server: On pourrait aussi s'en passer. Il s'agit simplement du serveur qui rend nos fichiers html, js et css dans le navigateur quand nous sommes en phase de dévelopement. On peut donc coder notre dévelopement sans s'occuper de monter un serveur node par exemple. Mais on pourrait le faire simplement, on appellerait la commande de compilation webpack sur npm start et on lancerait un serveur qui, sur "get /" renverrait le html lié par des "link" et "script" au css recompilé et au js recompilé.

### babel

Là encore on va regarder package.json, on a:
- babel/core: C'est le programme babel, on ne prend que le core parce que si on installe babel entièrement, on va aussi appeler cent autres modules qui ne nous servent à rien dans notre application.
- babel/preset-env: c'est l'outil appelé par le core qui permet à babel de traduire l'es6 en un javascript lisible par tous les navigateurs
- babel/preset-react: l'outil qui nous permet de compiler le jsx react en un js générant du html. Si on avait installer tout babel on aurait ces trois outils ainsi que ceux permettant par exemple de compiler vue, angular et milles autres trucs dont on a pas besoin ici. 
- babel-loader: c'est en fait un outil webpack qui va nous permettre de faire appeler babel par webpack

### react

On a react, le framework rendu disponible dans les dépendencies et react-dom pour lier react au dom, c'est la doc de react, je ne vais pas vraiment m'y attarder, mes premiers destinataires font plutôt du vue.js, pas moi, l'idée est plutôt de présenter une configuration minimal webpack plutôt qu'un webpack-react. 

Notons le react-hot-loader ... à destination de webpack, ce programme permet simplement de surveiller les changements appliqués au fichiers du dossier qu'il surveille et recompiler et relancer le serveur automatiquement. C'est notre nodemon pour react.

### le css

On ne va pas s'y attarder énormément pour le moment, style-loader, css-loader et extract-text-webpack-plugin nous servirons pour la gestion du css au sein de notre app. Ils ne sont pas nécessaires, mais sans eux, on va être obligé de linké tous nos css individuellement au fichier index.html rendu. C'est long, c'est chiant, c'est moche, au moindre changement de chemin d'un css faut le changer dans l'index.html ... nous ne sommes pas les esclaves de la machine ! J'y reviendrais plus tard.

## L'architecture

Nous avons maintenant nos outils installés, on va pouvoir créer notre architecture minimal. Nous sommes en développement, nous utilisons webpack-dev-server, notre racine est donc bien la racine du projet lui même. L'exemple ici est suffisant. 

Nous aurons besoin d'un fichier de configuration webpack en js dans lequel on dira à webpack ce qu'il doit compiler, comment le faire et avec quels outils. On l'installe à la racine, au niveau de notre package.json.
Par convention on appellera ce fichier "webpack.config.js".

On va créer un dossier src.
Ce dossier contient les sources de notre projet. Tout ce qu'on ne rendra pas directement au navigateur mais qu'on devra d'abord compiler.

Puis le dossier dist.
dist pour distribution. Ce qui sera vraiment rendu au navigateur. On va compiler nos composants JS en un ou plusieurs fichiers JS (un seul c'est mieux, moins de chargements mais il y a certainement quelques cas rares où on pourrait envisager la création de plusieurs JS), le ou les fichiers JS générés par cette compilation iront ici. Même chose pour le css et tout ce qui nécessitera compilation.

Si on avait du ajouter notre propre serveur on aurait pu ajouter à la racine un dossier server qui contiendrait par exemple avec node, notre app.js. Dans ce cas on aurait simplement app.get("/", (req, res) => res.end("../dist/index.html"))

## webpack.config.js

Allons y maintenant, la config webpack. Je vous invite à aller voir le fichier commenté pour bien comprendre comment il fonctionne.
Comme vous pouvez le constater, le fichier est un ensemble de clé. Je vais revenir sur les plus importantes ici:

### Entry et Output

Les clés entry et output permettent de définir les entrées et sorties de weback.
Notre entry, ici index.js est effectivement la porte d'entrée à tout notre système react. Comme le ferait un moteur d'éxecution de code, webpack va lire les lignes de nos fichiers les unes après les autres, inclure ce qui doit l'être, modifier les contenus qui doivent l'être. On prend donc le haut de la chaîne de notre programme afin que tout notre programme soit passé à webpack.
En sortie, webpack va créer un fichier, ici /dist/bundle.js.
Une fois ce fichier crée, on y retrouvera tout notre code, minifié et traduit pour le navigateur.

### Les modules

Les modules, ce sont nos transformations. Il s'agit d'une liste de règle indiquant à webpack comment se comporter avec les fichiers qu'il rencontre lors de son process de traduction. Ici on a deux règles: on traduit tous les fichiers qui finissent par .js grâce à babel et tous les fichiers qui finissent en .css grâce à css-loader et style-loader.
Si je fais cette seconde règle c'est parce qu'on aimerait, pour des raisons de lisibilité, que les css soient inclus dans les composants react et non pas attachés simplement à index.html dans le dur.

Si on récapitule ce qu'on sait, webpack va lire index.js, le fichier inclu App.js. Webpack va donc lire App.js, le fichier inclu App.css. Webpack va donc envoyer App.css à css-loader puis une fois fini la sortie de ce premier traitement ira à style-loader. App.js et index.js sont des fichiers js, ils sont envoyés à babel.

C'est très grossier mais c'est l'idée.

### Les plugins

Les plugins ajoutent à webpack des comportements qu'il n'a pas normalement. 

Normalement, webpack prend les fichiers et les rassemblent en un seul fichier au gré des includes. Pour le css, si on ne fait rien, comme le style est inclu dans du js, webpack essaiera de mettre nos règles css directement dans bundle.js ... mais comme le css, ce n'est pas du js, tout va foirer. On va donc utiliser un plugin pour changer ce comportement par défaut. On demandera au plugin webpack d'intercepter (d'extract-text) les .css et de les mettre dans un fichier style.css plutôt que de les inclure dans bundle.js.

Même chose pour hotModule, on change le comportement de webpack, on lui demande de recompiler à chaque changement.

PS: Pour le css, en webpack version 4, on devrait préférer le plugin mini-css-extract-plugin.

## Les commandes

### compilation
Pour bien comprendre, on va d'abord commencer par le npm run compilation.
Dans nos conditions, rien de plus simple, on appelle juste a commande webpack. Par défaut, webpack va chercher ses configuration dans le fichier webpack.config.json à la racine du projet, rien ne nous empêche cependant de changer le path des configs, je vous invite à aller voir la doc pour ça.
On précise qu'on veut ici compiler en mode development et c'est parti:

    webpack --mode development

Nos deux fichiers se créent correctement dans dist, on a un fichier style.css issu des css inclus dans nos js et le fichier bundle.js qui est la compilation de nos fichiers js. Ce sont ces fichiers qu'on devra fournir au navigateur.


### dev-server

Mais bon, on a déjà dit qu'on ne voulait pas créer nous même un server pour cette expérience en développement, alors on va utiliser webpack-dev-server.
Avant de commencer, supprimer les fichiers bundle.js et style.css qu'on vient de créer pour voir la différence:

    webpack-dev-server --mode development

Si tout se passe bien on reçoit correctement en console les notifications de compilation et le lancement du server ... mais aucun fichier n'est crée dans dist.
C'est tout simplement parce que webpack-dev-server se sert de sa mémoire vive, il n'écrit pas ces fichiers dans le disque dur. C'est sa tambouille interne, mais on se rassure, avec les outils de développement du navigateur, on se rend vite compte dans le débugger ou dans les descriptions réseaux que bundle.js et style.css sont bien appelés.

## Conclusion

Je découvre moi même webpack et la personnalisation des configurations. Ce document n'a pas vocation a être parfaitement exact concernant le fonctionnement interne de webpack, j'utilise parfois des raccourcis ou des explications grossières, ce que je veux faire passer, c'est les idées globales, le fait que, jeune, innocent et naïf développeur que nous somme, ce qu'on croit être au premier abord une usine à gaz n'est en réalité pas si compliqué et est vraiment là pour faciliter la vie. Pour le reste, il faut creuser, fouiller, lire les docs et rester curieux.







