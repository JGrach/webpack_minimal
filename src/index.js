import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader' // Pour vérifier les changements sur tous ce qui est contenu dans ce composant

import App from './App'


function renderApp() {
  render(
    <AppContainer>
        <App/>
    </AppContainer>,
    document.getElementById('app') // App.js ira dans la balise div id="app"
  )
}

renderApp()

if(module.hot){
  module.hot.accept();
}

