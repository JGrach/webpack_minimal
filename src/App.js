import React from 'react'
import './App.css'

const App = (props) => {
  return (
      <div id="router">
        <p> Salut tout le monde, je suis un composant react passé par webpack </p>
        <p> On peut voir que le <span id="red">style</span> est bien appliqué</p>
      </div>
  )
}

export default App